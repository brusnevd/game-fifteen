const SIZE = 4;

let moves = 0,
    time, min = 0,
    sec = 0,
    mas, randomArray, gameNumsArray, imageUrl, scores = [];

function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}

function getIndexOfZero(arr) {
    let len = arr.length,
        index = 0;
    for (let i = 0; i < len; i++) {
        for (let j = 0; j < len; j++) {
            if (arr[i][j] == 0) {
                index = i * 4 + j;
                return {
                    index: index,
                    i: i,
                    j: j
                };
            }
        }
    }
}

function checkIfGameFinished(mas) {
    let arr = [];

    for (let i = 0; i < mas.length; i++) {
        for (let j = 0; j < mas.length; j++) {
            arr.push(Number(document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText))
        }
    }

    if (arr[arr.length - 1] != 0) return false;

    for (let i = 0; i < arr.length - 2; i++) {
        if (arr[i] > arr[i + 1]) {
            return false;
        }
    }

    return true;
}

function putZeroInMas(i, mas) {
    for (let i = 0; i < SIZE; i++) {
        for (let j = 0; j < SIZE; j++) {
            mas[i][j] = 1;
        }
    }
    mas[i][randomInteger(0, 3)] = 0;

    return mas;
}

function checkIfThereIsSolution(mas) {
    let inv = 0,
        len = mas.length;

    for (let i = 0; i < len; i++) {
        if (mas[i]) {
            for (let j = 0; j < i; j++) {
                if (mas[j] > mas[i]) {
                    inv += 1;
                }
            }
        }
    }

    for (let i = 0; i < len; i++) {
        if (mas[i] == 0) {
            inv += 1 + i / 4;
        }
    }

    return inv & 1 ? false : true
}

function getRandomArray(nums) {
    let arr = [],
        len = nums.length;

    for (let i = 0; i < len; i++) {
        let index = Math.round(Math.random() * (14 - i));
        arr.push((nums[index]));
        nums.splice(index, 1);
    }

    return arr;
}

function createInitialTable(isGameContinue = false) {



    scores = [];

    let imgIndex = randomInteger(1, 150);

    // imgIndex = 53;

    imageUrl = "src/img/" + imgIndex.toString() + ".jpg";

    // console.log(localStorage.getItem("fifteenData"));

    moves = 0, min = 0, sec = 0;
    // -----------------------------------------------------------

    mas = [
        [1, 1, 1, 1],
        [1, 1, 1, 1],
        [1, 1, 1, 1],
        [1, 1, 1, 1]
    ]
    const NUMS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    let table = document.createElement('table');
    table.id = "game-area";
    // randomArray = NUMS;
    randomArray = getRandomArray(NUMS);
    mas = putZeroInMas(randomInteger(0, 3), mas);

    let zeroIndex = getIndexOfZero(mas).index;

    randomArray.splice(zeroIndex, 0, 0);

    if (!checkIfThereIsSolution(randomArray)) {
        if (getIndexOfZero(mas).i % 2 == 0) {
            mas = putZeroInMas([1, 3][randomInteger(0, 1)], mas);
            randomArray.splice(zeroIndex, 1);
            zeroIndex = getIndexOfZero(mas);
            randomArray.splice(zeroIndex.index, 0, 0);
        } else {
            mas = putZeroInMas([0, 2][randomInteger(0, 1)], mas);
            randomArray.splice(zeroIndex, 1);
            zeroIndex = getIndexOfZero(mas);
            randomArray.splice(zeroIndex.index, 0, 0);
        }
    }

    console.log("scores", scores)

    if (localStorage.getItem("fifteenScores")) {
        let localScores = JSON.parse(localStorage.getItem("fifteenScores"));
        scores = localScores.bestScores;
    }

    if (isGameContinue) {
        // if (localStorage.getItem("fifteenData") && localStorage.getItem("fifteenData") != "null") {
        let localData = JSON.parse(localStorage.getItem("fifteenData"));

        mas = localData.massive;
        randomArray = localData.numbers;
        moves = localData.moves;
        min = localData.minutes;
        sec = localData.seconds;
        imageUrl = localData.image;

        // console.log();
    } else {
        // localStorage.setItem("fifteenData", null)
    }

    console.log(randomArray, mas);

    // ------------------------------------------------------------------------------------------------------

    for (let i = 0; i < SIZE; i++) {
        let row = document.createElement('tr');
        for (let j = 0; j < SIZE; j++) {
            let col = document.createElement('td');
            col.classList.add('element' + i.toString() + j.toString())
            if (mas[i][j] != 0) {
                let newI = Math.ceil(randomArray[i * SIZE + j] / SIZE) - 1;
                let newJ = (randomArray[i * SIZE + j] - 1) % SIZE;
                console.log(randomArray[i * SIZE + j], newI, newJ);
                // console.log(randomArray[i * SIZE + j], newJ);
                col.innerText = (randomArray[i * SIZE + j]);
                col.classList.add('active');
                col.style.backgroundImage = 'url(' + imageUrl + ')';
                col.style.backgroundPosition = (400 - newJ * 100).toString() + 'px ' + (400 - newI * 100) + 'px';
            } else {
                col.classList.add('droppable');
            }
            // col.addEventListener('click', (event) => colClickHandler(event, i, j));
            // col.draggable = true;
            col.addEventListener('dragstart', () => false);
            col.addEventListener('mousedown', (event) => mousedownHandler(event, i, j));
            // col.addEventListener('drag');
            row.appendChild(col);
        }
        table.appendChild(row);
    }

    document.body.appendChild(table);

    function colClickHandler(event, i, j) {
        console.log('кликнутые - ', i, j)
        let arr = [];
        let text = '',
            toReturn = true;

        let moveX = [-1, 1],
            moveY = [-1, 1];

        if (i == 0) {
            moveX[0] = 0;
        }
        if (i == 3) {
            moveX[1] = 0;
        }
        if (j == 0) {
            moveY[0] = 0
        }
        if (j == 3) {
            moveY[1] = 0
        }
        if (mas[i + moveX[0]][j] == 0) {
            mas[i + moveX[0]][j] = 1;
            mas[i][j] = 0;
            text = document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText;
            render(text, i + moveX[0], j, i, j);
            moves += 1;
            for (let i = 0; i < mas.length; i++) {
                for (let j = 0; j < mas.length; j++) {
                    arr.push(Number(document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText))
                }
            }
            randomArray = arr.slice();
            saveInfoInLocalStorage()
            showMoves();
        } else
        if (mas[i + moveX[1]][j] == 0) {
            mas[i + moveX[1]][j] = 1;
            mas[i][j] = 0;
            text = document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText;
            render(text, i + moveX[1], j, i, j);
            moves += 1;
            for (let i = 0; i < mas.length; i++) {
                for (let j = 0; j < mas.length; j++) {
                    arr.push(Number(document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText))
                }
            }
            randomArray = arr.slice();
            saveInfoInLocalStorage()
            showMoves();
        } else
        if (mas[i][j + moveY[0]] == 0) {
            mas[i][j + moveY[0]] = 1;
            mas[i][j] = 0;
            text = document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText;
            render(text, i, j + moveY[0], i, j);
            moves += 1;
            for (let i = 0; i < mas.length; i++) {
                for (let j = 0; j < mas.length; j++) {
                    arr.push(Number(document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText))
                }
            }
            randomArray = arr.slice();
            saveInfoInLocalStorage()
            showMoves();
        } else
        if (mas[i][j + moveY[1]] == 0) {
            mas[i][j + moveY[1]] = 1;
            mas[i][j] = 0;
            text = document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText;
            render(text, i, j + moveY[1], i, j);
            moves += 1;
            for (let i = 0; i < mas.length; i++) {
                for (let j = 0; j < mas.length; j++) {
                    arr.push(Number(document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText))
                }
            }
            randomArray = arr.slice();
            saveInfoInLocalStorage()
            showMoves();
        } else {
            toReturn = false;
        }
        if (checkIfGameFinished(mas)) {
            showModalWin();
        }

        console.log(randomArray)

        saveInfoInLocalStorage()

        return toReturn;
    }

    function render(text, i2, j2, initI, initJ) {
        // i2, j2 - место где была пустая клетка до рендеринга
        // mas - уже изменен
        console.log('mas - ', mas, i2, j2)
        for (let i = 0; i < SIZE; i++) {
            for (let j = 0; j < SIZE; j++) {
                let element = document.getElementsByClassName('element' + i.toString() + j.toString())[0];
                if (mas[i][j] == 1) {
                    element.classList.add('active');
                    element.classList.remove('droppable');
                } else {
                    element.classList.add('droppable');
                    element.classList.remove('active');
                    element.innerText = '';
                    element.style.backgroundImage = '';
                    // element.style.backgroundImage = 'url(' + imageUrl + ')';
                    // element.style.backgroundPosition = (400 - initI * 100).toString() + 'px ' + (400 - initJ * 100) + 'px';
                }
                if (i == i2 && j == j2) {
                    element.innerText = text;
                    element.style.backgroundImage = "url(" + imageUrl + ")";
                    console.log("element with background image - ", element.style.backgroundImage)
                    element.style.backgroundPosition = document.getElementsByClassName('element' + initI.toString() + initJ.toString())[0].style.backgroundPosition;
                    // element.style.backgroundImage = '';
                    // element.style.backgroundImage = 'url(' + imageUrl + ')';
                    // element.style.backgroundPosition = (400 - initI * 100).toString() + 'px ' + (400 - initJ * 100) + 'px';
                }
            }
        }
    }

    function mousedownHandler(event, i, j) {
        let isMoved = false;

        let element = event.target;

        console.log(element.innerText)

        if (element.innerText == "") {
            return;
        }

        let elementCopy = document.createElement("td");

        audio.currentTime = 0;
        audio.play();

        elementCopy.innerText = element.innerText;
        elementCopy.style.display = 'flex';
        elementCopy.style.justifyContent = 'center';
        elementCopy.style.alignItems = 'center';

        elementCopy.style.backgroundImage = 'url(' + imageUrl + ')';
        elementCopy.style.backgroundPosition = element.style.backgroundPosition;

        let shiftX = event.clientX - element.getBoundingClientRect().left;
        let shiftY = event.clientY - element.getBoundingClientRect().top;

        elementCopy.style.position = 'absolute';
        elementCopy.style.zIndex = 1000;

        // console.log(document.getElementsByTagName('tr'))
        // for (let elem of document.getElementsByTagName('tr')) {
        //     elem.append(elementCopy);
        // }

        document.getElementsByTagName('tr')[0].appendChild(elementCopy);

        element.style.opacity = '0';

        function moveAt(pageX, pageY) {
            elementCopy.style.left = pageX - shiftX + 'px';
            elementCopy.style.top = pageY - shiftY + 'px';
        }

        moveAt(event.pageX, event.pageY);

        let currentDroppable = null;
        let droppableBelow, isElementOverZero = false;

        function onMouseMove(event) {
            isMoved = true;
            moveAt(event.pageX, event.pageY);

            elementCopy.style.display = 'none';
            let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
            elementCopy.style.display = 'block';
            elementCopy.style.display = 'flex';
            elementCopy.style.justifyContent = 'center';
            elementCopy.style.alignItems = 'center';
            elementCopy.style.cursor = 'pointer';
            elementCopy.style.backgroundImage = 'url(' + imageUrl + ')';
            // elementCopy.style.backgroundColor = '#FFFDF6';
            // elementCopy.hidden = false;
            if (!elemBelow) return;
            droppableBelow = elemBelow.closest('.droppable');

            // console.log(elemBelow)

            if (currentDroppable != droppableBelow) {
                if (currentDroppable) {
                    // логика обработки процесса "вылета" из droppable (удаляем подсветку)
                    // leaveDroppable(currentDroppable);
                    isElementOverZero = false;
                }
                currentDroppable = droppableBelow;
                if (currentDroppable) {
                    // логика обработки процесса, когда мы "влетаем" в элемент droppable
                    console.log('IN')
                    isElementOverZero = true;
                    // enterDroppable(currentDroppable);
                }
            }
        }

        document.addEventListener('mousemove', onMouseMove);

        elementCopy.onmouseup = function() {
            // if (!isMoved) {
            //     let isSomeColWasMoved = colClickHandler(" ", i, j);
            //     console.log(i, j)
            //     if (isSomeColWasMoved) {
            //         console.log(elementCopy)
            //         document.body.removeChild(elementCopy);
            //     }
            //     isMoved = false;
            // }
            if (!isMoved) {
                let isSomeColWasMoved = colClickHandler(" ", i, j);
                if (isSomeColWasMoved) {

                }
            } else {
                if (isElementOverZero) {
                    audio2.play();
                    let isSomeColWasMoved = colClickHandler(" ", i, j);
                }
            }
            // document.getElementById('game-area').removeChild(elementCopy);
            element.style.opacity = '1';
            document.getElementsByTagName('tr')[0].removeChild(elementCopy);
            document.removeEventListener('mousemove', onMouseMove);
            elementCopy.onmouseup = null;
        };
    }
}

function reloadGame(event) {
    if (event.keyCode == 82) {
        document.body.removeChild(document.getElementById("game-area"));
        document.body.removeChild(document.getElementById("game-stat"));
        createInitialTable();
        clearInterval(time);
        startTime(Number(min), Number(sec));
        showMoves();
    }
}

function showModalWin() {
    clearInterval(time);

    document.body.removeChild(document.getElementById("keyinfo"));

    window.removeEventListener('keydown', reloadGame);

    console.log(scores)

    scores.push({
        minutes: min,
        seconds: sec,
        moves: moves
    });

    saveInfoInLocalStorage();

    document.getElementById("game-area").style.position = 'relative';

    let info = document.getElementById("game-stat").innerText;

    console.log('time - ', document.getElementById("moves").innerText);

    let modal = document.createElement('div');

    modal.id = "modal-win"

    // document.body.style.position = 'relative';

    modal.style.width = '100%';
    modal.style.height = '100%';
    modal.style.background = 'url(' + imageUrl + ')';
    // modal.style.backgroundPosition = 'contains';
    modal.style.backgroundSize = '100%';
    modal.style.position = 'absolute';
    modal.style.fontSize = '20px';
    modal.style.fontFamily = 'sans-serif';
    modal.style.display = 'flex';
    modal.style.justifyContent = 'center';
    modal.style.alignItems = 'center';
    modal.style.border = '1px solid gray';
    modal.style.color = 'white';
    modal.style.textShadow = '0 0 20px black';
    modal.innerText = 'You won in ' + info;

    let close = document.createElement('span');
    close.id = 'close';
    close.style.position = 'absolute';
    close.innerText = 'Close';
    close.style.top = '10px';
    close.style.right = '10px';
    close.style.cursor = 'pointer';
    close.style.fontSize = '18px';
    close.style.fontFamily = 'sans-serif';
    close.style.zIndex = '999';
    close.style.color = 'white';

    modal.style.opacity = '0';

    close.addEventListener('click', (event) => {
        document.getElementById("game-area").removeChild(document.getElementById('modal-win'));
        document.getElementById("game-area").removeChild(document.getElementById('close'));
        localStorage.setItem("fifteenData", null);
        showMenu();
    });

    // document.getElementById("game-area").removeChild(document.getElementById("menu"));
    document.getElementById("game-area").prepend(modal);
    document.getElementById("game-area").prepend(close);
    document.getElementById("game-stat").style.display = 'none';

    let ind = 0.5;

    let modalOpac = setInterval(function() {
        if (ind == 10) {
            clearInterval(modalOpac);
        }
        ind += 0.5;
        modal.style.opacity = ind / 10;
    }, 50);

    // document.body.appendChild(modal)
    // document.body.appendChild(close)
}

function startTime(initialMinutes = 0, initialSeconds = 0) {
    let gameStat = document.createElement("div");
    gameStat.id = "game-stat";

    let timeBlock = document.createElement("span");
    let movesBlock = document.createElement("span");
    movesBlock.id = "moves";

    let zeroMinutes = "",
        zeroSeconds = "",
        minutes, seconds;

    timeBlock.id = "time";

    if (initialSeconds != 0 && initialSeconds % 60 == 0) {
        initialMinutes += 1;
    }
    if (initialSeconds < 10) {
        zeroSeconds = "0";
    } else {
        zeroSeconds = "";
    }
    if (initialMinutes < 10) {
        zeroMinutes = "0";
    } else {
        zeroMinutes = "";
    }
    minutes = zeroMinutes + initialMinutes;
    seconds = zeroSeconds + initialSeconds;
    timeBlock.innerText = "Time: " + minutes + ":" + seconds;

    document.body.appendChild(gameStat);
    gameStat.appendChild(timeBlock);
    gameStat.appendChild(movesBlock);

    movesBlock.innerText = " Moves: ";

    time = setInterval(() => {
        let arr = [];

        for (let i = 0; i < mas.length; i++) {
            for (let j = 0; j < mas.length; j++) {
                arr.push(Number(document.getElementsByClassName('element' + i.toString() + j.toString())[0].innerText))
            }
        }

        randomArray = arr.slice();

        initialSeconds += 1;
        if (initialSeconds != 0 && initialSeconds % 60 == 0) {
            initialSeconds = 0;
            initialMinutes += 1;
        }
        if (initialSeconds < 10) {
            zeroSeconds = "0";
        } else {
            zeroSeconds = "";
        }
        if (initialMinutes < 10) {
            zeroMinutes = "0";
        } else {
            zeroMinutes = "";
        }
        minutes = zeroMinutes + initialMinutes;
        seconds = zeroSeconds + initialSeconds;
        timeBlock.innerText = "Time: " + minutes + ":" + seconds;
        min = minutes;
        sec = seconds;
        // console.log(min, sec)
        saveInfoInLocalStorage();
    }, 1000);
}

function showMoves() {
    document.getElementById("moves").innerText = ' Moves: ' + moves;
}

function showScores() {
    scores.sort((a, b) => {
        return a.moves > b.moves ? 1 : -1;
    })

    scores.splice(10, scores.length - 9);

    let bestScores = document.createElement('div');

    // bestScores.id = 'scores';

    bestScores.style.position = 'absolute';
    bestScores.style.height = '100%';
    bestScores.style.width = '100%';
    bestScores.style.background = 'linear-gradient(#EDF8DC, #B8F8DC, #82F3BC)';
    bestScores.style.border = '1px solid gray';
    bestScores.style.display = 'flex';
    bestScores.style.flexDirection = 'column';
    bestScores.style.justifyContent = 'space-around';
    bestScores.style.alignItems = 'center';
    bestScores.style.flexDirection = 'column'
    bestScores.style.fontFamily = 'sans-serif';
    bestScores.style.fontWeight = 'bold';
    bestScores.style.fontSize = '15px';
    bestScores.style.color = '#F868A2';
    bestScores.style.textShadow = '0 0 1px gray';
    bestScores.style.zIndex = '9999';

    let back = document.createElement('div');
    console.log("scores in block - ", scores)
    bestScores.innerHTML = "<div>Top 10 scores by moves</div>";
    scores.forEach((elem, index) => {
        let zeroMin = "",
            zeroSec = "";
        let scoreItem = document.createElement("div");
        if (elem.minutes < 10) {
            zeroMin = "0";
        }
        if (elem.seconds < 10) {
            zeroSec = "0";
        }
        scoreItem.innerText = `${index + 1}. Time: ${zeroMin}${Number(elem.minutes)}:${zeroSec}${Number(elem.seconds)} Moves: ${elem.moves}`;
        bestScores.appendChild(scoreItem);
    });

    back.innerText = 'Back';
    back.style.cursor = 'pointer';

    back.addEventListener('click', () => {
        document.getElementById("game-area").removeChild(bestScores);
    });

    bestScores.appendChild(back);

    document.getElementById('game-area').style.position = 'relative';
    document.getElementById('game-area').prepend(bestScores);
}

function showMenu() {
    let mainMenuBlock = document.createElement('div');
    mainMenuBlock.id = "menu";

    if (document.getElementById('game-stat')) {
        document.body.removeChild(document.getElementById('game-stat'));
    }
    document.getElementById('game-area').style.position = 'relative';
    document.getElementById('game-area').prepend(mainMenuBlock);

    mainMenuBlock.style.position = 'absolute';
    mainMenuBlock.style.height = '100%';
    mainMenuBlock.style.width = '100%';
    mainMenuBlock.style.background = 'linear-gradient(#EDF8DC, #B8F8DC, #82F3BC)';
    mainMenuBlock.style.border = '1px solid gray';
    mainMenuBlock.style.display = 'flex';
    mainMenuBlock.style.justifyContent = 'center';
    mainMenuBlock.style.alignItems = 'center';
    mainMenuBlock.style.flexDirection = 'column'
    mainMenuBlock.style.fontFamily = 'sans-serif';
    mainMenuBlock.style.fontWeight = 'bold';
    mainMenuBlock.style.fontSize = '20px';
    mainMenuBlock.style.color = '#F868A2';
    mainMenuBlock.style.textShadow = '0 0 1px gray';
    mainMenuBlock.classList.add("main-menu");

    let menuItems = ["New Game", "Continue last Game", "Best Scores", "Settings"];

    menuItems.forEach((element, index) => {
        let item = document.createElement('div');
        item.innerText = element;
        item.style.textAlign = 'center';
        item.style.cursor = 'pointer';
        item.style.marginBottom = '20px';
        mainMenuBlock.appendChild(item);
        if (index == 0) {
            item.addEventListener('click', () => {
                mainMenuBlock.style.display = 'none';
                let reverseInfo = document.createElement("span");

                reverseInfo.innerText = "r - новая игра";
                reverseInfo.style.position = 'absolute';
                reverseInfo.style.top = '10px';
                reverseInfo.style.left = '10px';
                reverseInfo.id = 'keyinfo';
                document.body.prepend(reverseInfo);
                window.addEventListener('keydown', reloadGame);

                // document.getElementById('game-area').style.position = 'static';
                document.body.removeChild(document.getElementById('game-area'));
                createInitialTable();
                // startTime(Number(min), Number(sec), Number(moves));
                startTime(0, 0);
                showMoves();
            });
        } else if (index == 1) {
            if (localStorage.getItem("fifteenData") && localStorage.getItem("fifteenData") != "null") {
                item.addEventListener('click', () => {
                    mainMenuBlock.style.display = 'none';

                    let reverseInfo = document.createElement("span");
                    reverseInfo.innerText = "r - новая игра";
                    reverseInfo.style.position = 'absolute';
                    reverseInfo.style.top = '10px';
                    reverseInfo.style.left = '10px';
                    reverseInfo.id = 'keyinfo';
                    document.body.prepend(reverseInfo);
                    window.addEventListener('keydown', reloadGame);

                    // document.getElementById('game-area').style.position = 'static';
                    document.body.removeChild(document.getElementById('game-area'));
                    createInitialTable(true);
                    startTime(Number(min), Number(sec));
                    showMoves();
                });
            } else {
                item.style.cursor = "initial";
                item.style.userSelect = "none";
                item.classList.add("disable")
            }
        } else if (index == 2) {
            item.addEventListener('click', () => {
                showScores();
            });
        }
    });
}

function saveInfoInLocalStorage() {
    localStorage.setItem("fifteenData", JSON.stringify({
        minutes: min,
        seconds: sec,
        moves: moves,
        massive: mas,
        numbers: randomArray,
        image: imageUrl,
    }));
    localStorage.setItem("fifteenScores", JSON.stringify({
        bestScores: scores
    }));
}

function saveBestGamesInLocalStorage() {

}

if (localStorage.getItem("fifteenData") && localStorage.getItem("fifteenData") != "null") {
    createInitialTable(true);
    showMenu();
} else {
    createInitialTable();
    showMenu();
}

// createInitialTable();

var audio = document.createElement("audio");
audio.src = "src/sounds/tink.wav";
document.body.appendChild(audio);

var audio2 = document.createElement("audio");
audio2.src = "src/sounds/snare.wav";
document.body.appendChild(audio2);